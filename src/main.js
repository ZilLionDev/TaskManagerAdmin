import Vue from 'vue';
import App from './App';
import router from './router';
import store from './store';
import '@/assets/css/index.scss';
import Vuex from 'vuex';
import axios from 'axios';
import VueAxios from 'vue-axios';
import VueLazyload from 'vue-lazyload';
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'vue-awesome/icons';
import Icon from 'vue-awesome/components/Icon';
import NProgress from 'nprogress';
import 'nprogress/nprogress.css';
import VueCookie from 'vue-cookie';

Vue.use(VueCookie);
Vue.use(VueLazyload, {
  preLoad: 2,
  loading: require('./assets/img/loading.gif'),
  error: require('./assets/img/error.jpg'),
  attempt: 1
});
// 引入全局自定义组件 ==开始

Vue.component('v-icon', Icon);
Vue.config.productionTip = false;
Vue.use(ElementUI);
// 引入全局自定义组件 ==结束

router.beforeEach((to, from, next) => {
  NProgress.start(); // 开启Progress
  next();
});
router.afterEach(() => {
  NProgress.done(); // 结束Progress
});
Vue.use(VueAxios, axios);
Vue.use(Vuex);
axios.defaults.timeout = 30000; // 30S超时
axios.defaults.headers.post['Content-Type'] = 'application/json;charset=UTF-8';
console.log(process.env);
axios.defaults.baseURL = process.env.API_HOST;
// axios 拦截器
// 加上固定Token 参数
axios.interceptors.request.use((config) => {
  let cookie = require('vue-cookie');
  let jwt = cookie.get('zillionossjwt');
  if (jwt) {
    let params = config.params;
    if (params) {
      params.token = jwt;
    } else {
      params = {
        token: jwt
      };
    }
    config.params = params;
  }
  return config;
}, (error) => {
  return Promise.reject(error);
});

// 返回状态判断(添加响应拦截器)
axios.interceptors.response.use((res) => {
  return res;
}, (error) => {
  if (!error.response) {
    ElementUI.Message.error(error.response);
    /* router.push({name: 'error'}); */
  } else {
    if (error.response.status === 401) {
      VueCookie.delete('zillionossjwt');
      ElementUI.Message.error('Jwt验证失败,请重新打开页面!');
      /* router.push({name: 'customerror', query: {errdesc: 'Jwt验证失败,请重新打开页面!'}}); */
    } else {
      if (error.response.status === 404) {
        ElementUI.Message.error(error.response.status);
        let exceptionless = require('exceptionless');
        let client = new exceptionless.ExceptionlessClient({
          apiKey: 'XCSAkgV13RqjZj2vtCDKjIHuqoMZKPUQQPR9caoh',
          serverUrl: 'http://123.57.226.114:8004',
          submissionBatchSize: 100
        });
        client.submitNotFound(error.response.config.url);
      } else {
        ElementUI.Message.error(error.response);
        /* router.push({name: 'customerror', query: {errdesc: error.message}}); */
      }
    }
  }

  return Promise.reject(error);
});
// 全局axios配置初始化 ==>结束
// 全局错误处理
Vue.config.errorHandler = (err, vm, info) => {
  if (err) {
    let exceptionless = require('exceptionless');
    let client = new exceptionless.ExceptionlessClient({
      apiKey: 'i1CbrmsfFIYGBdsvVttq6zl9nODog5wiG0icuOql',
      serverUrl: 'http://123.57.226.114:8004',
      submissionBatchSize: 100
    });
    client.submitException(err);
  }
};
/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  template: '<App/>',
  components: {
    App
  }
});
