import Vue from 'vue';
import Router from 'vue-router';
import Layout from '@/views/layout/Layout';
import Index from '@/views/taskmanager/index';
import Plugin from '@/views/taskmanager/plugin';
import Task from '@/views/taskmanager/task';
import Node from '@/views/taskmanager/node';
import Addtask from '@/views/taskmanager/addtask';

const _import = require('./_import_' + process.env.NODE_ENV);
// in development env not use Lazy Loading,because Lazy Loading large page will cause webpack hot update too slow
// 所以只在生产中使用延迟加载
/* 404 */
const Err404 = _import('error/404');

// const dashboard = _import('home/dashboard');
Vue.use(Router);
/** 路由对象额外属性注释
 * icon : 菜单图标
 * hidden : true不显示在菜单栏
 * redirect : noredirect 为不重定向
 * nodropdown : true 不显示子菜单
 * isadmin : 是否系统管理员only(租户公司管理员都可以使用)
 * issystem : 是否系统管理页面(只有我司内部可以使用)
 * isconstant:是否是固定路由
 * componentpath :组件路径
 **/

// 路由表
export const routerMap = [
  {path: '/404', name: '404', component: Err404, hidden: true, isconstant: true},
  // 首页(固定的)
  {
    path: '/',
    component: Layout,
    redirect: '/index',
    module: "TaskPlatform",
    name: '调度平台管理',
    icon: 'bell',
    isconstant: 'true',
    children: [{
      isconstant: 'true',
      path: 'index',
      module: "TaskPlatform",
      component: Index,
      name: '调度平台监控'
    },
      {
        isconstant: 'true',
        path: 'plugin',
        module: "TaskPlatform",
        component: Plugin,
        name: '模块设置'
      },
      {
        isconstant: 'true',
        path: 'task',
        module: "TaskPlatform",
        component: Task,
        name: '任务设置'
      },
      {
        isconstant: 'true',
        path: 'node',
        module: "TaskPlatform",
        component: Node,
        name: '节点设置'
      },
      {
        isconstant: 'true',
        path: 'addtask',
        hidden: true,
        module: "TaskPlatform",
        component: Addtask,
        name: '录入任务'
      }
    ]
  }
];
export default new Router({
  // mode: 'history', //后端支持可开
  scrollBehavior: () => ({y: 0}),
  routes: routerMap
});
