require('./check-versions')()
// 根据外部传入process.env.NODE_ENV 判断使用哪一个配置文件
console.log(process.env.NODE_ENV);
switch (process.env.NODE_ENV) {
  case 'devrelease':
    var config = require('../config/index').devrelease;
    break;
  case 'preprod':
    var config = require('../config/index').preprod;
    break;
  case 'build':
    var config = require('../config/index').build;
    break;
  case 'local':
    var config = require('../config/index').localdev;
    break;
  case 'dev':
    var config = require('../config/index').dev;
    break;
  default:
    var config = require('../config/index').build;
}
console.log(config);
var ora = require('ora')
var rm = require('rimraf')
var path = require('path')
var chalk = require('chalk')
var webpack = require('webpack')
var webpackConfig = require('./webpack.prod.conf')
var spinner = ora('building for production...')
spinner.start();

rm(path.join(config.assetsRoot, config.assetsSubDirectory), err => {
  if (err) throw err
  webpack(webpackConfig, function (err, stats) {
    spinner.stop()
    if (err) throw err
    process.stdout.write(stats.toString({
      colors: true,
      modules: false,
      children: false,
      chunks: false,
      chunkModules: false
    }) + '\n\n')

    console.log(chalk.cyan('  Build complete.\n'))
    console.log(chalk.yellow(
      '  Tip: built files are meant to be served over an HTTP server.\n' +
      '  Opening index.html over file:// won\'t work.\n'
    ))
  })
})
