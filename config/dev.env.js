var merge = require('webpack-merge')
var prodEnv = require('./prod.env')

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_HOST: '"http://192.168.5.72:9085/api/ossgateway"',
  WS_API: '"ws://192.168.5.72:9085/websocket"'
});
